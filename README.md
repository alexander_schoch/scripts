# scripts

All scripts in my `~/.scripts` folder are saved on this repository. All scripts in separate folders contain own `README.md` files.

## Descriptions

- **autologin:** a script which is called upon startup of i3. On my system, I am logged in automatically, but the screen is being locked with this script.
- **autoremove:** removes all unnecessary packages from the arch linux system.
- **changeKeyboardLayout:** toggles the keyboard layout from `en_US` to `de_CH` and back. I have bound it to a key combination.
- **convert-mp3:** Converts all music files in the working directory to .mp3 files and deletes the original ones. If `convert-mp3 [Artist] [Album]` is used, it sets those attributes to all files.
- **convert-mp4:** converts every video file to a mp4 file. 
  - *Syntax:* `convert-mp4 /path/to/file.whatever /path/to/destination.mp4`
- **fortune-inline:** Uses fortune to get a random sentence and reloads it until it is short enough to fit my screen. I only use it to display a fortune output on the bottom of my screen, using polybar.
- **mpv-random:** Gets all music files in a folder and all subfolders, randomizes and plays them. Currently, it does not have the functionality I want it to have, as you cannot acces mpv commands and CTRL-C only skips to the next track.
- **music:** works similar to mpv-random, but also displays a cava terminal and displays the next played songs in another terminal window.
- **music-show:** A helper script which saves the next n songs in a separate file, which is being read by music.sh
- **screenlock_pixel:** similar to `screenlock.sh`, but a nice variant I found online.
- **screenlock:** A script to take a screenshot, blur it, edit a lock symbol over the image and lock the screen with it.
- **screen:** A script to manage multiple screens with i3. Type `screen left/right/duplicate` to show the second screen on the right/left or duplicate the screen.
- **sl-endless:** A super-stupid script to loop the sl command. You have to quit the terminal emulator in order to quit the program.
- **work:** Script for my luaLaTeX/mardown working setup in the working directory: Opens the pdf file, opens a small terminal for bibtex (and similar) and opens vim with the .tex file. If there are multiple documents in the folder, one has to add the name (with extension) of the file.

## Dependencies

- `screenfetch` and `i3lock-color` for `autologin`
- You have to use Arch Linux for `autoremove`
- You have to use i3 or i3-gaps for `changeKeyboardLayout`
- `ffmpeg` for `convert-mp3` and `convert-mp4`
- `id3v2` for `convert-mp3`, if `[Artist]` and `[Album]` are used
- `fortune` for `fortune-inline` 
- `i3lock`, `imagemagick` and `scrot` for `screenlock` and `screenlock_pixel`
- `mpv` for `mpv-random`
- `cava`, `terminator` and `music-show.sh` for `music`. You have to use i3 or i3-gaps for `music`
- `sl` for `sl-endless`
- `vim` and `okular` (and `pandoc`, if you use the mardown variant) for `work`. You have to use i3 or i3-gaps for `work`


