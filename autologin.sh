#!/bin/bash

xres=$( screenfetch | grep Resolution | tr 'x' '\n' | tr '\ ' '\n' | tail -n 2 | head -n 1 )

if [[ (( $xres > 2500 )) ]]
then
  i3lock -e -f -c 0a081a -i '/home/alexander_schoch/Pictures/i3lock/login_double.png' --insidevercolor=0a081aff --ringvercolor=0a081aff --insidecolor=0a081a99 --ringcolor=0a081aff --linecolor=0a081aff --keyhlcolor=a4e8f6ff --bshlcolor=0a081aff
else
  i3lock -e -f -c 0a081a -i '/home/alexander_schoch/Pictures/i3lock/login.png' --insidevercolor=0a081aff --ringvercolor=0a081aff --insidecolor=0a081a99 --ringcolor=0a081aff --linecolor=0a081aff --keyhlcolor=a4e8f6ff --bshlcolor=0a081aff
fi
