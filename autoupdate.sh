#!/bin/sh

# This script downloads the newest version of all upgradable packages and alerts the user afterwards. It is meant to be called regularily by a cronjob, e.g. every hour.

icon=/home/alexander_schoch/Pictures/Icons/arch.png

sudo pacman -Syuwq --noconfirm > /dev/null

num=$(sudo pacman -Qu | wc -l)

if [[ $num = 0 ]]
then
  notify-send -i $icon 'Arch Auto-Update' 'No packages can be upgraded right now.'
else
  if [[ $num = 1 ]]
  then
    notify-send -i $icon 'Arch Auto-Update' "1 package is ready to be installed."
  else
    notify-send -i $icon 'Arch Auto-Update' "$num packages are ready to be installed."
  fi
fi

