#!/bin/sh

icon=/home/alexander_schoch/Pictures/Icons/battery.png

[[ $( cat /sys/class/power_supply/BAT0/capacity ) -le 5 ]] && notify-send -i $icon 'Battery Status' "Only $( cat /sys/class/power_supply/BAT0/capacity )% left\nPlug me in! I know how much you hate it if I die due to battery..."

