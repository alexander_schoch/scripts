#!/bin/bash

output=$( setxkbmap -query | grep layout )
if [[ $output > "layout: ff" ]]
then
	setxkbmap ch
else 
	setxkbmap us
fi
