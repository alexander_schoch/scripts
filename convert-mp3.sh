#!/bin/bash

IFS=$(echo -en "\n\b")
for song in * ;
do
  if [ $( echo $song | grep mp3 | wc -l ) -eq 0 ] && [ $( echo $song | grep m4a | wc -l ) -eq 0 ] && [ $( echo $song | grep opus | wc -l ) -eq 0 ] && [ $( echo $song | grep wav | wc -l ) -eq 0 ] && [ $( echo $song | grep ogg | wc -l ) -eq 0 ] && [ $( echo $song | grep webm | wc -l ) -eq 0 ];
  then
    printf "$song\tThis is not an audio file. Skipping...\n"
    continue
  fi
  if [ $( echo $song | grep mp3 | wc -l ) -eq 1 ] 
  then
    if [ $# -eq 2 ]
    then
      id3v2 $song_conv -a $1 -A $2
    fi
    continue
  fi
  if [ $( echo $song | grep opus | wc -l ) -eq 1 ] || [ $( echo $song | grep webm | wc -l ) -eq 1 ]
  then
    echo "This is an opus/webm file. It has to be treated differently."
    song_conv=$( echo $song | head -c -5)mp3
  else
    song_conv=$( echo $song | head -c -4)mp3
  fi  
  ffmpeg -i $song $song_conv
  rm $song

  if [ $# -eq 2 ] 
  then
    # artist album
    id3v2 $song_conv -a $1 -A $2
  fi
done
