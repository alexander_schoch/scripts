#!/bin/bash

fort=$( fortune | tr '\n' ' ' )
while [ $( echo $fort | wc -m ) -gt $1 ]
do
  fort=$(fortune | tr '\n' ' ' )
done

echo $fort

