# Stream Music from Youtube the fancy way!

## What is this?

What you're seeing is a collection of scripts, designed to stream music in its best quality available from youtube. 

  - *bottom - User input:* The user searches youtube and picks an entry
  - *middle - Queue:* Management of the play queue
  - *top - Playing:* Plays the topmost entry 

## How do I use it?

1. Enter search keywords and press [Enter]. The more precise your search is, the more probable it is to find what you are looking for.
2. The script will now display some of the results it has found.
3. Enter the number of your choice and press [Enter]
4. Your choice should no be visible on the Queue Window (if there aren't more than 10 entries)

