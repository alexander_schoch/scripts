#!/bin/bash

QUEUE=/home/alexander_schoch/.scripts/jukebox/queue
NUM_ENTRIES=10

bold=$(tput bold)
normal=$(tput sgr0)

clear
num_ln=$( cat $QUEUE | wc -l )
echo -e "${bold}Those are the songs which will be played next (of $num_ln total):${normal}\n"
index=1
while read ln
do
  echo -e "\t($index) \t$( echo $ln | cut -f 2 -d @ )"
  [[ $index = $NUM_ENTRIES ]] && exit 0
  ((index++))
done < $QUEUE
