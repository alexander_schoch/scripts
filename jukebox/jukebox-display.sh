#!/bin/bash

QUEUE=/home/alexander_schoch/.scripts/jukebox/queue
HELPERSCRIPT=/home/alexander_schoch/.scripts/jukebox/jukebox-display-helper.sh

while [[ true ]]
do
  if [[ -e $QUEUE ]] 
  then
    #echo $QUEUE | entr echo -e "Those are the songs which will be played next:\n\n$( cat $QUEUE | cut -f 2 -d , )"
    echo $QUEUE | entr -c $HELPERSCRIPT 
  else
    clear
    printf "There is no entry on the list. You are free to add one\n"
    sleep 2
  fi
done
