#!/bin/bash

# Number of entries which should be displayed
NUM_URLS=6
QUEUE=/home/alexander_schoch/.scripts/jukebox/queue
TAB_SPACE=2
#term_width=$( tput cols )
term_width=82
bold=$(tput bold)
normal=$(tput sgr0)

# getting the url list out of the search keywords
clear
while [[ true ]] ; do
  printf "${bold}Please enter some keywords. Keep in mind to be as precise as possible.${normal}\n\n> "
  read usr_input
  [[ $usr_input = "exit" ]] && exit 0
  keywords=$( echo "$usr_input" | sed 's/\ /+/g' )
  webpage_reduced=$( curl -s "https://www.youtube.com/results?search_query=${keywords}&sp=EgIQAQ%253D%253D" | grep '/watch?v' | grep Duration | grep title | head -n $NUM_URLS )
  url_list=$( echo "$webpage_reduced" | sed 's/\ /\n/g' | grep '/watch?v' | cut -f 2 -d \" )
  name_list=$( echo "$webpage_reduced" | sed 's/</\n/g' | grep views | grep -v '^li' | cut -f 2 -d \> | sed 's/amp;//g' | sed "s/\&\#39;/\'/g" | sed 's/\&quot;/\"/g' )
  duration_list=$( echo "$webpage_reduced" | sed 's/</\n/g' | grep Duration | sed 's/\ /\n/g' | grep "\." | cut -f 1 -d . )
  uploader_list=$( echo "$webpage_reduced" | sed 's/span>/\n/g' | grep 'byline' | sed 's/>/\n/g' | grep '/a' | cut -f 1 -d \< )
  #url_list=$( curl -s "https://www.youtube.com/results?search_query=${keywords}&sp=EgIQAQ%253D%253D" | grep '/watch?v' | sed 's/\ /\n/g' | grep 'watch?v' | cut -f 2 -d \" | uniq | head -n $NUM_URLS )

  # Downloading Video Titles and the duration
  #echo -e "\n\tThe script is now downloading video titles. Please wait while they are appearing.\n"
  echo -e "\n\tThe script is now displaying the first $NUM_URLS entries it has found:\n"
  index=1
  names=""
  while read -r ln ; do
    url="https://youtube.com${ln}"
    #filename="$( youtube-dl -q --restrict-filenames --get-filename $url -o '%(title)s@%(duration)d' )"
    #name="$( echo $filename | sed 's/_/\ /g' | cut -f 1 -d @ )"
    name=$( echo "$name_list" | head -n $index | tail -n 1 )
    duration=$( echo "$duration_list" | head -n $index | tail -n 1 )
    uploader=$( echo "$uploader_list" | head -n $index | tail -n 1 )
    names="${names}@${index}_${name}"

    [[ ${#duration} < 5 ]] && duration="0$duration" 
    #sec=$( echo $filename | cut -f 2 -d @ )
    #min=$(( $(( $sec - $(( $sec % 60 )) )) / 60 ))
    #sec=$(( $sec - ( 60 * $min )))
    #[[ ${#min} < 2 ]] && min="0$min" 
    #[[ ${#sec} < 2 ]] && sec="0$sec"
    #h=$(( ( $min - ( $min % 60 )) / 60 ))
    #min=$(( $min - ( 60 * $h )))
    #echo -e "\t($index/$NUM_URLS) - ($duration) $name"
    echo -e "\t($index/$NUM_URLS)"
    max_width=$(( $term_width - $(( $TAB_SPACE * 2 )) - 15 ))
    [[ ${#name} -gt $max_width ]] && echo -e "\t\t- Title:    $( echo $name | cut -c 1-${max_width} )..." || echo -e "\t\t- Title:    $name"
    echo -e "\t\t- Duration: $duration"
    echo -e "\t\t- Uploader: $uploader"
    ((index++))
  done <<< "$url_list"

  names="$( echo "$names" | sed 's/@/\n/g' )"

  # Ask the user to pick an entry
  printf "\n${bold}Now, please choose a number:${normal}\n\n> "
  read choice
  index=1
  success=0

  # Write the matching entry to the queue file
  while read -r ln ; do
    if [[ $index = $choice ]] 
    then
      printf "https://youtube.com${ln}@" >> $QUEUE && success=1
      echo "$names" | grep "^$choice" | cut -f 2 -d _ >> $QUEUE
    fi
    ((index++))
  done <<< "$url_list"
  if [[ $success = 0 ]] 
  then
    echo -e "\n${bold}[Error]: The value you entered is either not a value or not in the given range${normal}" 
    sleep 2
  else
    echo -e "\nadded choice $choice to the list" 
    sleep 2
  fi
  clear
done
