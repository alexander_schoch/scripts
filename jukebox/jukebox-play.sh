#!/bin/bash

QUEUE=/home/alexander_schoch/.scripts/jukebox/queue
bold=$(tput bold)
normal=$(tput sgr0)


clear
while [[ true ]] ; do 
  sleep 1
  printf "This is the reader script. If nothing is playing, nothing as been sent in...\n\n" 
  while [[ -z $play ]] ; do 
    sleep 1 && [[ -e $QUEUE ]] || continue
    index=1
    # Taking the top most link and save it in $play
    while read url ; do
      if [[ $index = 1 ]] 
      then
        play=$url 
      else 
        echo $url >> "${QUEUE}_tmp"
      fi
      ((index++))
    done < $QUEUE
  done
  rm $QUEUE  
  if [[ -e "${QUEUE}_tmp" ]]
  then
    mv "${QUEUE}_tmp" "$QUEUE"
  fi
  # Get Video name
  #name="$( youtube-dl -q --restrict-filenames --get-filename $play -o '%(title)s' | sed 's/_/\ /g' | cut -f 1 -d \. )"
  name=$( echo $play | cut -f 2 -d @ )
  link=$( echo $play | cut -f 1 -d @ )
  echo -e "Currently playing: ${bold}$name${normal}\n\n"
  # Play the song
  mpv --ytdl-format=best --no-video --load-unsafe-playlists --ytdl-raw-options=force-ipv4= --term-playing-msg='now playing: \e[1m\e[4m${media-title}' $link 
  unset play name link
  clear
done
