#!/bin/bash

FOLDER=$1

TEMPLATEFILE=~/.local/merge_ical/template.ics
sed -e '$d' $TEMPLATEFILE
for file in $FOLDER/*/*.ics
do
  sed -e '1,/VEVENT/{/VEVENT/p;d}' $file | head -n -1
done
echo END:VCALENDAR

