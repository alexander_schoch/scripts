#!/bin/bash

wd=$(pwd)
ls -R1 > /tmp/mpv-random-ls.txt
rm /tmp/mpv-random-files.txt
touch /tmp/mpv-random-files.txt

file=$wd
file+=/
echo "working directory: $wd"
while read l; do
  if [[ $(echo $l | head -qc 1 ) = . ]]
  then
    if [[ $l = .: ]]
    then 
      continue 
    fi
    file=$wd
    file+=$(echo $l | tail -c +2 | head -c -2)
    file+=/
    # echo $file
    continue
  fi
  
  #mfile=\"
  mfile=$file
  mfile+=$l
  #mfile+=\"
  if [[ $(echo $mfile | tail -c 4 | head -c -1) = mp3 ]] || [[ $(echo $mfile | tail -c 4 | head -c -1) = wav ]] || [[ $(echo $mfile | tail -c 4 | head -c -1) = m4a ]] || [[ $(echo $mfile | tail -c 5 | head -c -1) = opus ]]
  then
    echo $mfile >> /tmp/mpv-random-files.txt
  fi
done </tmp/mpv-random-ls.txt

shuf /tmp/mpv-random-files.txt | sed 's/ /\\ /g' > /tmp/mpv-random-files-rand.txt
#echo "" > /tmp/mpv-random-files-rand-filenames.txt
# 
#while read l; do
#  [[ $( echo "$l" | head -c 1 ) = "." ]] && continue
#  [[ $( echo "$l" ) = "\n" ]] && continue
#  while read f; do
#    echo ""
#    [[ $( echo "$f" | grep "$l" ) -eq 1 ]] && echo $l >> /tmp/mpv-random-files-rand-filenames.txt
#    [[ $( echo "$f" | grep "$l" | wc -l ) -eq 1 ]] && printf "$l\n\n"
#  done < /tmp/mpv-random-files-rand.txt
#done < /tmp/mpv-random-ls.txt

while read f;
do
  mpv --no-video "$f" 
done </tmp/mpv-random-files-rand.txt
