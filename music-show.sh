#!/bin/bash

num_song=$( cat /tmp/music-show.txt | wc -l )
old_song=$( cat /tmp/music-playing.txt )

while true 
do
  if [[ $old_song != $( cat /tmp/music-playing.txt ) ]]
  then
    cat /tmp/music-show.txt > /tmp/music-show-tmp.txt
    while [[ $( cat /tmp/music-show-tmp.txt | head -n 1 ) != $( cat /tmp/music-playing.txt ) ]]
    do
      cat /tmp/music-show-tmp.txt | tail -n +2 > /tmp/music-show-tmp2.txt
      mv /tmp/music-show-tmp2.txt /tmp/music-show-tmp.txt
    done 
    cat /tmp/music-show-tmp.txt | head -n $1 > /tmp/music-show-tmp2.txt
    mv /tmp/music-show-tmp2.txt /tmp/music-show-tmp.txt
    rm /tmp/music-show/tmp2.txt
  fi
  clear
  echo -e "\E[1;34m"
  echo -e "\033[4mNext $( cat /tmp/music-show-tmp.txt | wc -l ) played songs:\033[0m"
  echo -e "\E[1;36m\n"
  cat /tmp/music-show-tmp.txt 
  old_song=$( cat /tmp/music-playing.txt )  
  sleep 1
done
