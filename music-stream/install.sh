#!/bin/bash

SOURCE_CODE="https://gitlab.com/alexander_schoch/scripts/-/raw/master/music-stream/music-stream"
DATAFILE="https://gitlab.com/alexander_schoch/scripts/-/raw/master/data/music-stream.csv"
mkdir -p ~/.scripts
mkdir -p ~/.scripts/data
wget -q "$SOURCE_CODE" -O ~/.scripts/music-stream
wget -q "$DATAFILE" -O ~/.scripts/data/music-stream.csv
chmod +x ~/.scripts/music-stream
sudo ln -s ~/.scripts/music-stream /usr/local/bin/

echo "==========="
echo "music-stream installed. use \"music-stream help\" to get more information about how to use it"
echo "==========="
music-stream help
