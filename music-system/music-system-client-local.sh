#!/bin/bash

FALLBACK=~/Sync/Music/PartyPlaylist

QUEUE=~/.scripts/music-system/server/queue.csv
DEFAULTQUEUE=/tmp/defaultqueue.txt

bold=$(tput bold)
normal=$(tput sgr0)

ls -R1 $FALLBACK | shuf > $DEFAULTQUEUE

while [[ True ]]
do
  data=$( cat $QUEUE | head -n 1 ) 
  sed -i '1d' $QUEUE
  if [[ ! -z $data ]]
  then  
    name=$( echo $data | cut -d ',' -f 3 )
    link=$( echo $data | cut -d ',' -f 1 )
    echo "Submitted by ${bold}$name${normal}:" 
    mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --term-playing-msg='now playing: \e[1m\e[4m${media-title}' "$data"
  else
    echo "from ${bold}Default-Playlist${normal}:" 
    file=$( cat $DEFAULTQUEUE | head -n 1 ) 
    sed -i '1d' $DEFAULTQUEUE
    mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --term-playing-msg='now playing: \e[1m\e[4m${media-title}' "$FALLBACK/$file"
  fi
done
