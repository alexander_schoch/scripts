#!/bin/bash

RASPI='192.168.1.106'
FALLBACK=~/Sync/Music/PartyPlaylist

bold=$(tput bold)
normal=$(tput sgr0)

while [[ True ]]
do
  data=$( ssh pi@$RASPI -p 10600 'cat /var/www/html/queue.txt | head -n 1 && /usr/local/bin/rm-first-line.sh' ) ; \
  if [[ ! -z $data ]]
  then  
    name=$( echo $data | cut -d ',' -f 3 )
    link=$( echo $data | cut -d ',' -f 1 )
    echo "Eingereicht von ${bold}$name${normal}:" 
    mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --term-playing-msg='now playing: \e[1m\e[4m${media-title}' "$link" 
  else
    echo "von der ${bold}Default-Playlist${normal}:" 
    file="$( ls -R1 $FALLBACK | shuf | head -n 1 )"
    mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --term-playing-msg='now playing: \e[1m\e[4m${media-title}' "$FALLBACK/$file"
  fi
done
