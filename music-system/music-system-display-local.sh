#!/bin/bash

QUEUE=~/.scripts/music-system/server/queue.csv

bold=$(tput bold)
normal=$(tput sgr0)

IFS=,

index=1

while read ln
do
  name=$( echo "$ln" | cut -d ',' -f 3 )
  title=$( echo "$ln" | cut -d ',' -f 2 )
  printf "($index) $name\n" 
  printf "    $title\n" 
  ((index++))
done < $QUEUE
