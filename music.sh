#!/bin/bash

i3-msg split v
st -e cava &
#i3-msg resize shrink height 50px
#i3-msg resize shrink height 50px
i3-msg focus up

## Basically a copy of mpv-random.sh

wd=$( pwd )
ls -R1 > /tmp/music-ls.txt
printf "" > /tmp/music-files.txt
printf "" > /tmp/music-show.txt

file=$wd
file+=/
echo "working directory: $wd"
while read l; do
  if [[ $(echo $l | head -qc 1 ) = . ]]
  then
    if [[ $l = .: ]]
    then 
      continue 
    fi
    file=$wd
    file+=$(echo $l | tail -c +2 | head -c -2)
    file+=/
    continue
  fi
  
  mfile=$file
  mfile+=$l
  if [[ $(echo $mfile | tail -c 4 | head -c -1) = mp3 ]] || [[ $(echo $mfile | tail -c 4 | head -c -1) = wav ]] || [[ $(echo $mfile | tail -c 4 | head -c -1) = m4a ]] || [[ $(echo $mfile | tail -c 5 | head -c -1) = opus ]]
  then
    echo $mfile >> /tmp/music-files.txt
  fi
done </tmp/music-ls.txt

shuf /tmp/music-files.txt | sed 's/ /\\ /g' > /tmp/music-files-rand.txt

while read line
do
  echo $line | sed 's/\//\n/g' | tail -n 1 >> /tmp/music-show.txt
done < /tmp/music-files-rand.txt

printf "" > /tmp/music-playing.txt
i3-msg split h 
#st -e /home/alexander_schoch/.scripts/music-show.sh 10

while read f;
do
  echo $f | sed 's/\//\n/g' | tail -n 1 > /tmp/music-playing.txt
  mpv --no-video "$f" 
done </tmp/music-files-rand.txt



