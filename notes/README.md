# notes

## Description

This is a script to manage note taking in lectures and exercises. When starting the script, it automatically gets the timetable from `timetable.csv` and knows, in what lecture I am sitting. It creates a note markdown file for each lecture/exercie and stores it in a lecture specific folder which is synchronized with gitlab. All notes should be written the way that they can be compiled using pandoc and LaTeX.

## Syntax

- see `notes -h` or `notes --help` for syntax explanation

## Dependencies 

- you have to use `i3` for `notes` itself. `notes info [parameters]` do not need it.
