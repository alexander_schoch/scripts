#!/bin/bash

notesDir='/home/alexander_schoch/.git/course-notes/'
timetable='/home/alexander_schoch/.scripts/notes/timetable.csv'
template='/home/alexander_schoch/.scripts/notes/template.md'
tmp='/tmp/notes_availableCourses.txt'

thisminute=$( date +%M )
thishour=$( date +%k )
thisday=$( date +%a )
thisdayfull=$( date +%A )
thisdaynumber=$( date +%d )
thismonthnumber=$( date +%m )
thismonth=$( date +%B )
thisyear=$( date +%G )

[[ $( echo $@ | grep -e '\-n' -e '\-\-no-first-semester' | wc -l ) -gt 0 ]] || with_1st=1
[[ $( echo $@ | grep -e '\-h' -e '\-\-help' | wc -l ) -gt 0 ]] && echo -e "notes.sh help:\n\n\t- use 'notes' to take notes.\n\t- use 'notes info today' to see the schedule for today\n\t- use 'notes info week' to see the schedule for the whole week\n\nOptions:\n\t-h, --help: Display this help and exit\n\t-n, --no-first-semester: Do not display first semester courses" && exit 0


IFS=,
printf "" > $tmp



if [[ $1 = 'info' ]] 
then
  if [[ $2 = 'week' ]]
  then
    echo -e "This is your weekly schedule:"
    while read day time lecture type place semester room
    do
      [[ -z $day ]] && continue
      [[ -z $with_1st ]] && [[ $semester = '1' ]] && continue
      [[ $day != $last_day ]] && echo ""
      [[ $type = 'Hand-in' ]] && echo -e "\t--> $day, $time: You'll have to hand in your \"$lecture\" Exercise at $room" && continue
      last_day=$day
      time_m1=$(( $time - 1 ))
      time_p1=$(( $time + 1 ))
      [[ $semester = 1 ]] && semester="1st"
      [[ $semester = 3 ]] && semester="3rd"
      [[ $( echo $time | wc -c ) -eq 2 ]] && asdf=" $time" && time=$asdf
      [[ $( echo $time_m1 | wc -c ) -eq 2 ]] && asdf=" $time_m1" && time_m1=$asdf
      [[ $( echo $time_p1 | wc -c ) -eq 2 ]] && asdf=" $time_p1" && time_p1=$asdf
      [[ $place = 'H' ]] && printf "\t- $day, $time_m1:45 - $time:30: $semester. semester $type of \"$lecture\" in $room\n"
      [[ $place = 'Z' ]] && printf "\t- $day, $time:15 - $time_p1:00: $semester. semester $type of \"$lecture\" in $room\n"
    done < $timetable
    exit 0
  fi
  if [[ $2 = 'today' ]]
  then
    echo -e "Today, you have the following courses:\n"  
    while read day time lecture type place semester room
    do
      [[ -z $day ]] && continue
      [[ -z $with_1st ]] && [[ $semester = '1' ]] && continue
      [[ $thisday != $day ]] && continue
      [[ $thisday = $day ]] && [[ $type = 'Hand-in' ]] && echo -e "\t--> $day, $time: You'll have to hand in your \"$lecture\" Exercise at $room" && continue
      time_m1=$(( $time - 1 ))
      time_p1=$(( $time + 1 ))
      [[ $semester = 1 ]] && semester="1st"
      [[ $semester = 3 ]] && semester="3rd"
      [[ $( echo $time | wc -c ) -eq 2 ]] && asdf=" $time" && time=$asdf
      [[ $( echo $time_m1 | wc -c ) -eq 2 ]] && asdf=" $time_m1" && time_m1=$asdf
      [[ $( echo $time_p1 | wc -c ) -eq 2 ]] && asdf=" $time_p1" && time_p1=$asdf
      [[ $place = 'H' ]] && printf "\t- $time_m1:45 - $time:30: $semester. semester $type of \"$lecture\" in $room\n"
      [[ $place = 'Z' ]] && printf "\t- $time:15 - $time_p1:00: $semester. semester $type of \"$lecture\" in $room\n"
    done < $timetable
    exit 0
  fi
  echo -e "notes.sh help:\n\n\t- use 'notes' to take notes.\n\t- use 'notes info today' to see the schedule for today\n\t- use 'notes info week' to see the schedule for the whole week\n\nOptions:\n\t-h, --help: Display this help and exit\n\t-n, --no-first-semester: Do not display first semester courses" && exit 0
fi

while read day time lecture type place semester room
do
  # Skip if day is null (and so is the whole line)
  [[ -z $day ]] && continue
  [[ $type = 'Hand-in' ]] && continue

  # Skip if the day does not match
  [[ $thisday != $day ]] && continue

  # Write the available lectures into a file in $tmp
  ## If the Lecture is in Zentrum, the lecture time is the same as the actual time. 
  [[ $place = Z ]] && [[ $thishour = $time ]] && echo "$day,$time,$lecture,$type,$place,$semester" >> $tmp
  ## If the lecture is in Hoengg, the lecture time is either the same as the actual time or an hour back
  [[ $place = H ]] && (( $thisminute < 30 )) && [[ $thishour -eq $time ]] && echo "$day,$time,$lecture,$type,$place,$semester" >> $tmp 
  [[ $place = H ]] && (( $thisminute > 30 )) && [[ $thishour -eq $(( $time - 1 )) ]] && echo "$day,$time,$lecture,$type,$place,$semester" >> $tmp 
done < $timetable


# If there is not entry in $tmp, print a nice message and exit the script
[[ -s $tmp ]] || { 
  echo "--- You don't seem to have a course now, so there is no need to take notes. Enjoy your 'free time'! ---" 
  exit 0 
}


# Ask the user if there is more than one course available
if [[ $( cat $tmp | wc -l ) -gt 1 ]] 
then
  printf "As there are multiple possible courses right now, notes.sh asks you to enter the index of your actual one.\n\n"
  index=1
  while read day time lecture type place semester
  do
    placeFull=$( [[ $place = H ]] && echo "Hönggergerg" || echo "Zentrum" )
    printf "\t($index)\t$type of '$lecture' at $placeFull from Semester $semester\n"  
    ((index++))
  done < $tmp
  echo ""
  printf "Please enter the desired Number:\n\n> "
  read choice
  echo "$( cat $tmp | head -n $choice | tail -n 1 )" > $tmp
fi

while read day time lecture type place semester room
do
  courseName=$( echo $lecture | sed 's/\ //g' )
  [[ ! -d  $notesDir$courseName ]] && mkdir $notesDir$courseName
  filename="$( echo $courseName )_$( date -I )_$( [[ $( echo $time | wc -c ) -eq 1 ]] && echo "0$time" || echo $time )_$type.md"
  file="$notesDir$courseName/$filename"
  echo $file
  [[ ! -f $file ]] && echo "---" > $file && printf "title: $type Notes - $lecture\nauthor: Alexander Schoch\ndate: $thisdayfull, $thismonth $thisdaynumber\n" >> $file && cat $template >> $file
  cd $notesDir$courseName

  ### Work.sh, but included

  fileroot=$( echo $file | head -c -4 )
  echo $fileroot
  pandoc -o "$fileroot.pdf" "$fileroot.md"
  i3-msg split h
  okular "$fileroot.pdf" &
  delay 1 
  i3-msg split v  
  terminator
  i3-msg resize shrink height 50 px
  i3-msg resize shrink height 50 px
  i3-msg resize shrink height 0 px
  i3-msg focus left
  vim - -c ":edit $fileroot.md"
done < $tmp








#match=0


#while IFS=, read day time name type place semester
#do
#  [[ $thisday != $day ]] && continue
#
#  if [[ $place = "H" ]]
#  then
#    thishour=$( echo $thistime | tr ':' '\n' | head -n 1)
#    thisminute=$( echo $thistime | tr ':' '\n' | head -n 2 | tail -n 1)
#    hour=$( [[ $thisminute -gt 30 ]] && echo $(( $thishour + 1 )) || echo $thishour )
#  else
#    thishour=$( echo $thistime | tr ':' '\n' | head -n 1)
#    hour=$thishour 
#  fi
#
#  [[ $hour != $time ]] && continue
#
#  match=1
#  
#  nameCC=$( echo $name | sed 's/\ //g' )
#  [[ ! -d  $notesDir$nameCC ]] && mkdir $notesDir$nameCC
#
#  filename="$( echo $nameCC )_$( date -I )_$( [[ $( echo $time | wc -c ) -eq 1 ]] && echo "0$time" || echo $time )_$type.md"
#  
#  file="$notesDir$nameCC/$filename"
#
#  [[ ! -f $file ]] && echo "---" > $file && printf "title: $type Notes - $name\nauthor: Alexander Schoch\ndate: $thisday $thismonth $thisdaynumber\n---\n\n\\maketitle\n" >> $file
#
#  cd $notesDir$nameCC
#  work
#   
#done < /home/alexander_schoch/.scripts/notes/timetable.csv
#
#[[ $match -eq 0 ]] && echo "--- You don't seem to have a course now, so there is no need to take notes. Enjoy your 'free time'! ---"


