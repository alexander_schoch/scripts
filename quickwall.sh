#!/bin/bash

wal -i $1 

# rofi
cp ~/.config/rofi/config.default ~/.config/rofi/config

colbg=$( cat ~/.cache/wal/colors | head -n 1 | tail -c 7 )
colfg=$( cat ~/.cache/wal/colors | head -n 5 | tail -n 1 )
sed -i 's/BG/#aa'$colbg'/g' ~/.config/rofi/config
sed -i 's/FGNORM/#ffffff/g' ~/.config/rofi/config
sed -i 's/FGSEL/'$colfg'/g' ~/.config/rofi/config
sed -i 's/LINE/'$colfg'/g' ~/.config/rofi/config

feh --bg-center $1

~/.telegram-palette-gen/telegram-palette-gen --wal

sed -i /SchemeUrg/d /home/alexander_schoch/.cache/wal/colors-wal-dwm.h
cd ~/.config/dwm
rm *.o
make
sudo make install

cd ~/.config/st
rm *.o
make
sudo make install

pkill dwm

#reTheme $(cat $HOME/.cache/wal/wal)
