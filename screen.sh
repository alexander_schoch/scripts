#!/bin/bash

if [[ $1 = "duplicate" ]]
then
  xrandr --output HDMI-2 --same-as eDP-1
else
  if [[ $1 = "left" ]]
  then 
    xrandr --output HDMI-2 --auto --primary --left-of eDP-1
  else
    if [[ $1 = "right" ]]
    then
      xrandr --output HDMI-2 --auto --primary --right-of eDP-1
    else
      echo "---You either did not give an argument or your argument is invalid.---"
    fi
  fi
fi
