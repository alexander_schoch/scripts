#!/bin/bash


# Do not start i3lock if mpv is running.

if pgrep -x "mpv" > /dev/null 
then
  exit 0
fi

scrot /tmp/screenshot_i3lock.png
amount=0x8
dark=0a081aff
bright=a4e8f6ff
darkt=0a081a99

colors="--insidevercolor=$dark --ringvercolor=$dark --insidecolor=$darkt --ringcolor=$dark --linecolor=$dark --keyhlcolor=$bright --bshlcolor=$dark"

# If I have a screen connected to my notebook, the script has to cut the screenshot into two different images, edit them and merge them again.

if [[ $( file /tmp/screenshot_i3lock.png | tr '\ ' '\n' | tr ',' ' ' | grep -v P | grep -v a | grep -v x | grep -v i | grep -v R | head -n 1 ) -gt 2000 ]]
then
  if [[ -f /tmp/screenshot_i3lock.png ]] 
  then
    convert /tmp/screenshot_i3lock.png -crop 1920x1080+1920+0 /tmp/screenshot_i3lock_cut2.png
    convert /tmp/screenshot_i3lock.png -crop 1920x1080+0+0 /tmp/screenshot_i3lock_cut1.png
    convert /tmp/screenshot_i3lock_cut1.png -blur $amount /home/alexander_schoch/Pictures/i3lock/lock.png -gravity center -composite -matte /tmp/screenshot_i3lock_edit1.png 
    convert /tmp/screenshot_i3lock_cut2.png -blur $amount /home/alexander_schoch/Pictures/i3lock/lock.png -gravity center -composite -matte /tmp/screenshot_i3lock_edit2.png
    convert +append /tmp/screenshot_i3lock_edit?.png /tmp/screenshot_i3lock_edit.png
  fi
else
  [[ -f /tmp/screenshot_i3lock.png ]] && 
    #convert /tmp/screenshot_i3lock.png -blur $amount /home/alexander_schoch/Pictures/i3lock/lock.png -gravity center -composite -matte /home/alexander_schoch/Pictures/i3lock/corners/tl_color_rsz.png -gravity NorthWest -geometry +25+25 -composite -matte /home/alexander_schoch/Pictures/i3lock/corners/br_color_rsz.png -gravity SouthEast -geometry +25+25 -composite -matte /home/alexander_schoch/Pictures/i3lock/corners/bl_color_rsz.png -gravity SouthWest -geometry +25+25 -composite -matte /home/alexander_schoch/Pictures/i3lock/corners/tr_color_rsz.png -gravity NorthEast -geometry +25+25 -composite -matte /tmp/screenshot_i3lock_edit.png 
    convert /tmp/screenshot_i3lock.png -blur $amount /home/alexander_schoch/Pictures/i3lock/lock.png -gravity center -composite -matte /tmp/screenshot_i3lock_edit.png 
fi


i3lock -e -f -c 0a081a -i /tmp/screenshot_i3lock_edit.png $colors
rm /tmp/screenshot_i3lock*
