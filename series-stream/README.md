# series-stream 

Script to stream and download all series from the terminal. The current watching progress is saved and can be modified. 

## Syntax

`series-stream [seriesName] [mode] [opts]`.

Modes:

- `series-stream [seriesName] play`: stream saved episode
- `series-stream [seriesName] set [season] [episode]`: Set watching progress
- `series-stream [seriesName] read`: Get the saved watching progress 
- `series-stream [seriesName] download`: Download saved episode

Note that series names are written with dashes (-) instead of spaces. Example: `series-stream how-i-met-your-mother play` 

## Dependencies
  
- mpv
- curl
