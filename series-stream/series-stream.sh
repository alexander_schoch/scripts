#!/bin/bash

urlroot="https://www.watchepisodeseries.com/"
savedir=~/.series-stream/
season=1
episode=1

mkdir -p $savedir 

if [ ! -f "$savedir$1" ]
then
  echo "--- The save file for this series did not exist. Creating one... ---"
  printf "# Season:\n$season\n\n# Episode:\n$episode\n" > "$savedir$1"
fi

if [ $# -gt 1 ] && [ $2 = "set" ] 
then
  echo "--- Saving Season $3, Episode $4 as watching progress... ---"
  printf "# Season:\n$3\n\n# Episode:\n$4\n" > "$savedir$1"
  exit 0
else
  if [ $# -eq 2 ] && [ $2 = "read" ]
  then
    echo "--- Current watching progress: ---"
    cat $savedir$1
    exit 0
  fi
fi


season=$(cat "$savedir$1" | grep -v '#' | head -n 1)
episode=$(cat "$savedir$1" | grep -v '#' | tail -n 1)

link=$(curl -s "$urlroot$1" | grep "$1-season-$season-episode-$episode-" | tail -c +54 | head -c -4)
link2=$(curl -s $link | grep "$1-season-$season-episode-$episode-" | grep speedvid | head -n 1 | head -c -20 | tail -c +38)
link3=$(curl -s $link2 | grep "speedvid.net" | grep href | tail -c +61 | head -c -84)

if [ $# -eq 2 ] && [ $2 = "download" ]
then
  echo "--- downloading Season $season, Episode $episode... ---"
  youtube-dl -v -R infinite $link3 
  id=$( echo $link3 | tail -c +25 )
  mv "video-${id}.mp4" "$1-S${season}E${episode}.mp4" 
  printf "# Season:\n$season\n\n# Episode:\n$(( $episode + 1 ))\n" > "$savedir$1"
  exit 0
else
  if [ $# -eq 2 ] && [ $2 = "play" ]
  then
    echo "--- streaming Season $season, Episode $episode... ---"
    mpv --quiet --fs $link3
    printf "# Season:\n$season\n\n# Episode:\n$(( $episode + 1 ))\n" > "$savedir$1"
    exit 0
  fi
fi









#/ episodename=$(sudo curl "$urlroot$1/$season/" | grep -i "serie/$1/$season/$episode" | grep -i vivo | tr '/' '\n' | grep -i "$episode-")
#  
# bsout=$(sudo curl "$urlroot$1/$season/$episodename/OpenLoad" | grep -i "bs.to/out/" | tr ' ' '\n' | grep "bs.to/out" | grep href | tr '\"' '\n' | grep https)
# sudo curl $bsout > /tmp/series-stream/curl/bsout.txt
