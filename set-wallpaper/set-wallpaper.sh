#!/bin/bash

[[ -z $1 ]] && /home/alexander_schoch/.scripts/set-wallpaper/set-wallpaper.sh $( cat /home/alexander_schoch/.cache/wal/wal ) &> /dev/null
 
if [[ $1 = "random" ]]
then
  keywords="/home/alexander_schoch/.scripts/set-wallpaper/keywords.txt"
  alphacoders_search="https://wall.alphacoders.com/search.php?search="
  alphacoders_urlroot="https://wall.alphacoders.com/"
  alphacoders_download="https://initiate.alphacoders.com/download/wallpaper/"
  image_dir="/home/alexander_schoch/Pictures/Wallpapers/set-wallpaper/"

  if [[ -n $2 ]]
  then
    keyword=$2
  else
    keyword=$( cat $keywords | grep -v '#' | shuf -n 1 )
    echo "--- Keyword/s: $keyword ---"
  fi

  mkdir -p /tmp/set-wallpaper/
  curl $alphacoders_search$keyword -s > /tmp/set-wallpaper/alphacoders_search.txt
  max_page=$( cat /tmp/set-wallpaper/alphacoders_search.txt | grep 'Last Page' | head -n 1 | sed 's/\ /\n/g' | grep ')' | head -c 3 | tail -c 2 ) max_page=$( curl $alphacoders_search$keyword -s | grep 'Last Page' | head -n 1 | sed 's/\ /\n/g' | grep ')' | head -c 3 | tail -c 2 )
  [[ $( echo $max_page | tail -c -2 ) = ')' ]] && asdf=$max_page && max_page=$( echo $asdf | head -c 1 )
  [[ $max_page != 1 ]] && page=$(( ( RANDOM % $(( $max_page - 1 )) + 1 ) )) || page=1
  # curl "$alphacoders_search$keyword&page=$page" -s > /tmp/set-wallpaper/alphacoders_search.txt
  # img_line=$( cat /tmp/set-wallpaper/alphacoders_search.txt | grep 'big.php' | shuf | head -n 1 )
  img_line=$( curl "$alphacoders_search$keyword&page=$page" -s | grep 'big.php' | shuf | head -n 1 )
  id=$( echo $img_line | sed 's/\ /\n/g' | grep href | head -c 22 | tail -c 6 )
  title=$( echo $img_line | sed 's/=/\n/g' | grep "HD" | head -c -34 | tail -c +2 )
  img_line2=$( curl "$alphacoders_search$keyword&page=$page" -s | grep $id | grep "Download Wallpaper" | head -n 1 )
  server=$( echo $img_line2 | sed 's/\ /\n/g' | grep "data-server" | sed 's/\"/\n/g' | tail -n 2 )
  filetype=$( echo $img_line2 | sed 's/\ /\n/g' | grep "data-type" | sed 's/\"/\n/g' | tail -n 2 )
  # curl $alphacoders_urlroot/big.php?i=$id -s > /tmp/set-wallpaper/alphacoders_rsz.txt
  mkdir -p $image_dir
  wget -q "${alphacoders_download}${id}/${server}/${filetype}" -O $image_dir$id
  # wget -q $( cat /tmp/set-wallpaper/alphacoders_rsz.txt | grep 'btn btn-success btn-custom' | sed 's/\ /\n/g' | grep href | head -c -3 | tail -c +12 ) -O $image_dir$id
  echo "--- setting \"$title\" with ID $id as wallpaper ---"
fi


alpha=90
cache_dir=/home/alexander_schoch/.cache/wal


[[ $1 != random ]] && wal -a $alpha -i $1 &>/dev/null || wal -a $alpha -i $image_dir$id &>/dev/null

foreground=$( echo $( cat $cache_dir/colors | tail -n 1 | sed 's/\#/\#CC/g'  ) )
foreground_opaque=$( echo $( cat $cache_dir/colors | tail -n 1 ) )
foregroundalt=$( echo $( cat $cache_dir/colors | head -n 2 | tail -n 1 | sed 's/\#/\#CC/g'  ) )
foregroundalt_opaque=$( echo $( cat $cache_dir/colors | head -n 2 | tail -n 1 ) )
background=$( echo $( cat $cache_dir/colors | head -n 1 | sed 's/\#/\#CC/g'  ) )
background_opaque=$( echo $( cat $cache_dir/colors | head -n 1 ) )
backgroundalt=$( echo $( cat $cache_dir/colors | tail -n 2 | head -n 1 | sed 's/\#/\#CC/g'  ) )
empty="#00000000"

# Bspwm colors

bspwm_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/bspwm/bspwmrc
cat ${bspwm_conf}_base > $bspwm_conf
echo "bspc config normal_border_color = $( cat $cache_dir/colors | head -n 1 | tail -n 1 ) " >> $bspwm_conf
bspc config normal_border_color "$( cat $cache_dir/colors | head -n 1 | tail -n 1 )"
echo "bspc config active_border_color = $( cat $cache_dir/colors | head -n 1 | tail -n 1 ) " >> $bspwm_conf
bspc config active_border_color "$( cat $cache_dir/colors | head -n 1 | tail -n 1 )"
echo "bspc config focused_border_color = $( cat $cache_dir/colors | head -n 2 | tail -n 1 ) " >> $bspwm_conf
bspc config focused_border_color "$( cat $cache_dir/colors | head -n 2 | tail -n 1 )"

# Polybar colors

polybar_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/polybar/config
echo "[colors]" > $polybar_conf
echo "background = $( cat $cache_dir/colors | head -n 1 | sed 's/\#/\#CC/g' ) " >> $polybar_conf
echo "background-alt = $( cat $cache_dir/colors | tail -n 2 | head -n 1 ) " >> $polybar_conf
echo "foreground = $( cat $cache_dir/colors | tail -n 1 ) " >> $polybar_conf
echo "foreground-alt = $( cat $cache_dir/colors | head -n 2 | tail -n 1 ) " >> $polybar_conf
echo "border = $( cat $cache_dir/colors | tail -n 2 | head -n 1 ) " >> $polybar_conf
cat "${polybar_conf}_base" >> $polybar_conf

#[[ $( $( kill -0 $( cat /home/alexander_schoch/.config/polybar/PID ) ) | grep "process" | wc -l ) = 1 ]] && polybar mainbar &
#if [[ $( ps -p $( cat /home/alexander_schoch/.config/polybar/PID ) | wc -l ) = 1 ]] 
#then
#  polybar mainbar -q & 
#  echo $! > /home/alexander_schoch/.config/polybar/PID
#fi

killall polybar -q 
for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
  MONITOR=$m polybar mainbar 2> /dev/null &
  MONITOR=$m polybar fortune 2> /dev/null &
  MONITOR=$m polybar desktops 2> /dev/null &
done
# URxvt colors

urxvt_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.Xdefaults
cat ${cache_dir}/colors.Xresources > $urxvt_conf
cat ${urxvt_conf}_base >> $urxvt_conf



# rofi colors

rofi_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/rofi/config
cat "${rofi_conf}_base" > $rofi_conf
echo "rofi.color-normal: $empty, $foreground, $empty, $empty, $foregroundalt" >> $rofi_conf
echo "rofi.color-urgent: $empty, $foreground, $empty, $empty, $backgroundalt" >> $rofi_conf
echo "rofi.color-active: $empty, $foreground, $empty, $empty, $backgroundalt" >> $rofi_conf
echo "rofi.color-window: $background, $foregroundalt, $background" >> $rofi_conf

[[ $1 != random ]] && wal -a $alpha -i $1 &>/dev/null || wal -a $alpha -i $image_dir$id &>/dev/null


# okular colors

okular_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/okularpartrc
echo "[Dlg Accessibility]" > $okular_conf
echo "HightlightLinks=true" >> $okular_conf
echo "RecolorBackground=$background" >> $okular_conf
echo "RecolorForeground=$foregroundalt" >> $okular_conf
cat "${okular_conf}_base" >> $okular_conf
echo "BackgroundColor=$background" >> $okular_conf
echo "ShowScrollBars=false" >> $okular_conf
echo "UseCustomBackgroundColor=true" >> $okular_conf

# glava colors

glava_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/glava/bars.glsl
cat "${glava_conf}_base" > ${glava_conf}
echo "#define COLOR ($foregroundalt_opaque * GRADIENT * ALPHA)" >> ${glava_conf}

killall glava -q 
#glava &> /dev/null &



# terminator colors

terminator_conf=/home/alexander_schoch/.git/dotfiles-gentoo/.config/terminator/config
cat "${terminator_conf}_base" > ${terminator_conf}
echo "    background_color = \"${background_opaque}\"" >> ${terminator_conf}
echo "    foreground_color = \"${foreground_opaque}\"" >> ${terminator_conf}
printf "    palette = \"" >> ${terminator_conf}
while read p ; do
  printf $p >> ${terminator_conf}
  printf ":" >> ${terminator_conf}
done < "${cache_dir}/colors"
printf "\"" >> ${terminator_conf}

killall polybar -q 2> /dev/null 
for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
  MONITOR=$m polybar mainbar 2> /dev/null & 2> /dev/null 
  MONITOR=$m polybar fortune 2> /dev/null & 2> /dev/null 
  MONITOR=$m polybar desktops 2> /dev/null & 2> /dev/null 
done

