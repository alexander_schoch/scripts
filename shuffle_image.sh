#!/bin/bash

IMG_DIR=$1
OUTPUT=$2

convert -size 1920x1080 xc:#F6F4F3 output.png
sxiv output.png &

for i in $(seq 1 1)
do
  echo "Iteration $i"
  randimage="$( ls $1/*.png | shuf | head -n 1 )"
  randx=$((RANDOM % 1920))
  randy=$((RANDOM % 1080))
  randrot=$((RANDOM % 90 - 45))
  echo $randx $randy
  convert output.png \( $randimage -trim -rotate $randrot -resize x300+100+100 \) -composite output.png
  #composite ( -resize x300 -geometry +200+10 $randimage ) white.png output.png
#composite -geometry  +5+10 balloon.gif composite.gif composite.gif
done

sleep 1
pkill sxiv
