#/bin/sh

TMP_DIR='/home/alexander_schoch/.templates/'
DATA='/home/alexander_schoch/.scripts/data/template.csv'


#DEFAULT='file'

#[[ -z $1 ]] && FILENAME=$DEFAULT || FILENAME=$1

# Ask the user which entry to remove
printf "Please choose what file type to create:\n\n"
index=1
IFS=,
while read name path 
do
  [[ $( echo $name | grep "^#" | wc -l ) -eq 1 ]] && continue
  printf "\t($index) \t$name\n" 
  ((index++))
done < $DATA 
printf "\n> "
read choice
index=1
chosen=0
while read name_loop path_loop
do
  [[ $( echo $name_loop | grep "^#" | wc -l ) -eq 1 ]] && continue
  [[ $index = $choice ]] && path=$path_loop && name=$name_loop && chosen=1
  ((index++))
done < $DATA

[[ chosen -eq 0 ]] && echo "no filetype chosen" && exit 1

printf "please enter a filename:\n\n> "
read filename

printf "\ncreating $name file $filename \n"
cp "$TMP_DIR$path" "$PWD/$filename"
