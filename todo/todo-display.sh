#!/bin/bash

# This script displays all pending TODOs

# Files
DATA=~/.local/share/todo/todo.csv


# Entries with Deadline
echo -e "\e[1mentries with deadline:\e[0m"
while read ln
do
  date=$( echo "$ln" | awk -F "," '{print $1}' ) 
  tag=$( echo "$ln" | awk -F "," '{print $2}' ) 
  imp=$( echo "$ln" | awk -F "," '{print $3}' ) 
  name=$( echo "$ln" | awk -F "," '{print $4}' )
  [[ $imp -eq 1 ]] && echo -ne "\033[0;31m"
  echo -n "- "
  echo -n $( echo -n "$date" | tail -c 2 )
  echo -n '.'
  echo -n $( echo -n "$date" | tail -c 4 | head -c 2 )
  echo -n '.'
  echo -n $( echo -n "$date" | head -c 2 )
  echo -e "\t[$tag]\t$name"
  echo -ne "\033[0;37m"
done < <( sort -n "$DATA" | grep -v "^none" )

# Entries without Deadline
echo -e "\n\e[1mentries without deadline:\e[0m"
while read ln
do
  tag=$( echo "$ln" | awk -F "," '{print $2}' ) 
  imp=$( echo "$ln" | awk -F "," '{print $3}' ) 
  name=$( echo "$ln" | awk -F "," '{print $4}' ) 
  [[ $imp -eq 1 ]] && echo -ne "\033[0;31m"
  echo -n "-"
  echo -ne "\t[$tag]\t$name"
  echo -e "\033[0;37m"
done < <( sort -n "$DATA" | grep "^none" )
