#!/bin/bash

# ======== 
# This script handles the todo entries 
# ========

# Paths
DATA=~/.local/share/todo/todo.csv
DISPLAY=~/.scripts/todo/todo-display.sh
DONE=~/.local/share/todo/todo_done.csv


# Variables
bold=$(tput bold)
normal=$(tput sgr0)

# Tags
declare -a TAGS
TAGS=( ETH VCS Alt PlB Prv Oth VSE )


# Script start

case $1 in
"add")
  echo -ne "${bold}Please enter the name of the entry${normal}\n> "
  read entryname

  # Tag
  echo -ne "\n${bold}What category does the entry belong to?${normal}\n"
  index=1
  for tag in "${TAGS[@]}"
  do
    echo -ne "\t($index)\t$tag\n"
    ((index++))
  done
  echo -ne "> "
  read tagnum
  entrytag="${TAGS[$((tagnum - 1))]}"
  [[ -z $entrytag ]] && echo "Please enter a number from the list" && exit 1

  # Importance
  echo -ne "\nIs your entry especially \e[3mimportant\e[0m? (y/N)\n> "
  read imp
  [[ $imp == "y" ]] && entryimportance=1 || entryimportance=0

  # Deadline
  echo -ne "\nDoes your entry have a deadline? (y/N)\n> "
  read dl
  if [[ $dl = "y" ]] 
  then
    entrydeadline=1 
    echo -ne "\nWhen is the deadline? (yymmdd)\n> "
    read entrydeadlinedate
  else
    entrydeadline=0
  fi

  # Print to file
  [[ $entrydeadline -eq 1 ]] && echo "$entrydeadlinedate,$entrytag,$entryimportance,$entryname" >> $DATA || echo "none,$entrytag,$entryimportance,$entryname" >> $DATA
  ;;
"remove")
  # Ask the user which entry to remove
  printf "Please choose which entry to delete:\n\n"
  index=1
  IFS=,
  while read dl tag imp name 
  do
    echo -ne "\t($index) [$tag] $name\n" 
    ((index++))
  done < $DATA
  printf "\n> "
  read choice
  index=1
  while read line 
  do
    [[ $index != $choice ]] && echo "$line" >> tmp.csv
    ((index++))
  done < $DATA
  rm $DATA
  mv tmp.csv $DATA
  exit 0
  ;;
"done")
  # Ask the user which entry to mark as 'done'
  printf "Please choose which entry to mark as 'done':\n\n"
  index=1
  IFS=,
  while read dl tag imp name 
  do
    echo -ne "\t($index) [$tag] $name\n" 
    ((index++))
  done < $DATA
  printf "\n> "
  read choice
  index=1
  while read line 
  do
    [[ $index != $choice ]] && echo "$line" >> tmp.csv|| echo "$line" >> $DONE
    ((index++))
  done < $DATA
  rm $DATA
  mv tmp.csv $DATA
  exit 0
  ;;
"help")
  # Print help and exit
  echo -e "This script helps me managing my assignments. Commands:\n\n- todo help: print this help message and exit\n- todo add: add a new entry to the TODO list\n- todo remove: remove an entry from the TODO list\n- todo done: mark an entry as 'done'\n\nEverything else will just print out the TODO list."
  ;;
*)
  # Print out the todo list with the display script
  /bin/bash $DISPLAY
  ;;
esac


