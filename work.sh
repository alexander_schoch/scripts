#!/bin/bash

wm=$( wmctrl -m | head -n 1 | sed 's/\ /\n/g' | tail -n 1 )
split_ratio_bspwm=0.8
[[ $wm = 'bspwm' ]] && term=urxvt || term=terminator
templates_dir=/home/alexander_schoch/.templates/defaults/

if [[ -n $1 ]]
then
  [[ $( echo $1 | grep ".tex" ) ]] && filetype=tex && file_without=$( echo $1 | head -c -5 ) && file_with=$1
  [[ $( echo $1 | grep ".md" ) ]] && filetype=md && file_without=$( echo $1 | head -c -4 ) && file_with=$1
else
  echo "--- Please give a file (with ending) until the script was improved ---"
  exit 1
fi

function resize_i3
{
  i3-msg resize shrink height 50 px
  i3-msg resize shrink height 50 px
  i3-msg resize shrink height 0 px
  i3-msg focus left
}

function compile
{
  # $1 = file without ending, $2 = filetype
  [[ -n $2 ]] && filetype=$2
  [[ $filetype = 'tex' ]] && lualatex $1
  [[ $filetype = 'md' ]] && pandoc -o $file.pdf $file.md
}

function place_windows
{
  # Function to open the windows. $1 = file.whatever, $2 = filetype
  # Compile an existing file
  # compile $1 $2
  if [[ $wm = 'bspwm' ]]
  then
    compile $1 ; okular "$file_without.pdf" &
    sleep 1
    bspc node -p south -o $split_ratio_bspwm
    $term &> /dev/null &
    vim $file_with
  else
    if [[ $wm = 'i3' ]]
    then
      i3-msg split h
      okular "$file.pdf" & 
      delay 0.3
      i3-msg split v  
      $term #-e "cd $PWD" & 
      resize_i3
      vim "$file_with"
    fi
  fi
}



if [[ $( ls | grep ".tex" | grep -v synctex | grep -v '.tex~' | wc -l ) = 1 ]] && [[ $( ls | grep ".md" | wc -l ) = 0 ]]
then
  # There is exactly one .tex file and no .md file in the folder
  file_without=$( ls | grep '.tex' | grep -v synctex | grep -v ".tex~" | head -c -5 )
  echo "--- A .tex file was found: ${file_without}.tex ---" 
  place_windows ${file_without} tex
else
  if [[ $(ls | grep tex | grep -v synctex | wc -l) = 0 ]] && [[ $( ls | grep ".md" | wc -l ) = 1 ]]
  then
    # There is exactly one .md file and no .tex file in the folder
    echo "--- A Markdown file was found ---"
    file=$(ls | grep md | head -c -4)
    echo "$file.pdf"
    place_windows $file
  else  
    if [[ $filetype = "" ]]
    then
      echo ""
      echo "--- please choose a file to work on (with the .tex/.md ending) ---"
      echo ""
      exit
    else
      if [[ $( ls | grep $1 | wc -l ) = 0  ]]
      then 
        echo "--- The file you gave me does not exist. creating one... ---"
        [[ $filetype = 'tex' ]] && cat "${templates_dir}latex" > $file_with 
        [[ $filetype = 'md' ]] && cat "${templates_dir}markdown" > $file_with 
      fi
      place_windows $file_without
    fi
  fi
fi


