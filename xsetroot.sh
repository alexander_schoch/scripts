#!/bin/bash

export DISPLAY=:0

CPU="CPU $( mpstat -P ALL | awk '{print $4}' | tail -n 8 | cut -d . -f 1 | sed ':a;N;$!ba;s/\n/ /g' )"
VOL="VOL $( [[ $(pamixer --get-mute) = false ]] && echo $(pamixer --get-volume)% || echo mute )"
ch=$( [[ $(cat /sys/class/power_supply/BAT0/status) = Charging ]] && echo " C" || echo "" )
BAT="BAT$ch $( cat /sys/class/power_supply/BAT0/capacity )%"
DAT="$( date +'%a, %b %d 20%C, %H:%M')"
MEM="MEM $( free -m | grep Mem | awk '{print ($3/$2)*100}' | cut -d . -f 1 )%"
KBL="KBL $( setxkbmap -query | grep layout | tail -c 3)"

xsetroot -name " $DAT │ $BAT │ $VOL │ $CPU │ $MEM │ $KBL "
