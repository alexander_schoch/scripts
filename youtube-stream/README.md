# youtube-stream
Stream and download Youtube Videos from the terminal using search keywords

## Syntax
- `./youtube-stream.sh [keyword 1] [keyword 2] [etc.]` for streaming with video
- `./youtube-stream.sh download [keyword 1] [keyword 2] [etc.]` for downloading with video
- `./youtube-stream.sh audio [keyword 1] [keyword 2] [etc.]` for streaming without video
- `./youtube-stream.sh download audio [keyword 1] [keyword 2] [etc.]` for downloading without video

The keywords `download` and `audio` do not have to be at shown places in the commands. As soon as the words appear in the command, they are recognised.

Please note that it is advised not to use special characters. Just write `dont` instead of `don't`, as the script relies on the youtube search algorithm which also accepts bad spelling.

## Dependencies
- mpv
- curl 
