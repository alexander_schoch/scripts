#!/bin/bash

search=$( echo "$@" | sed "s/audio//" | sed "s/download//" | sed "s/playlist//" | sed "s/\ \ /\ /" | tr ' ' '+' )
if [[ $( echo $search | tail -c 2 ) = "+" ]]
then
  search=$( echo $search | head -c -2 )
fi
if [[ $( echo $search | head -c 1 ) ]]
then
  search=$( echo $search | tail -c +2 )
fi


if [ $( echo $@ | grep playlist | wc -l ) -eq 1 ]
then
  curl -s "https://www.youtube.com/results?sp=EgIQA0IECAESAA%253D%253D&search_query=$search" | grep '/watch?v' > /tmp/youtube-stream-ids.txt
else
  curl -s "https://www.youtube.com/results?search_query=$search" | grep '/watch?v' > /tmp/youtube-stream-ids.txt
fi

href=$(head -n 1 /tmp/youtube-stream-ids.txt | tr ' ' '\n' | grep 'href=')
id=$( echo $href | tail -c +7 | head -c -2 )

audio=false
download=false
playlist=false

if [ $( echo $@ | grep download | wc -l ) -eq 1 ]
then
  if [ $( echo $@ | grep audio | wc -l ) -eq 1 ]
  then
    youtube-dl -x https://youtube.com/$id
  else
    youtube-dl https://youtube.com/$id
  fi
else
  if [ $( echo $@ | grep audio | wc -l ) -eq 1 ]
  then
    mpv --no-video https://youtube.com/$id --ytdl-raw-options=force-ipv4=
  else
    mpv https://youtube.com/$id --ytdl-raw-options=force-ipv4=
  fi
fi

